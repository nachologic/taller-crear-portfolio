---
title: "Cómo crear tu portfolio con Hugo y desplegarlo"
author: "Nacho Loyola - nacho@nachologic.com"
date: "Hackerspace Valencia"
theme: "Copenhagen"
---

# Copia de estas diapositivas

Puedes encontrar una copia de estas diapositivas en el siguiente enlace:

[https://gitlab.com/hackerspacevlc1/taller-crear-portfolio](https://gitlab.com/hackerspacevlc1/taller-crear-portfolio)

---

# ¿Qué veremos en este workshop?

1. ¿Qué es un portfolio? ¿Para qué sirve?
2. ¿Qué es Hugo? ¿Por qué usarlo?
3. Preparativos iniciales
4. Crear un nuevo proyecto con Hugo
5. Personalizar el tema
6. Desplegar el portfolio en Cloudflare
7. Conclusiones

---

![](./images/hackerspace-valencia.jpg)

---

# ¿Qué es un portfolio? ¿Para qué sirve?

### Qué es un portfolio

Un portfolio es una colección de trabajos o proyectos que has realizado. Puede ser una colección de fotografías, de diseños, de proyectos de programación, etc.

### Para qué sirve

Un portfolio sirve para mostrar tus habilidades y tus trabajos a otras personas. Es una forma de mostrar tu trabajo de una forma visual y atractiva.

---

# ¿Qué es Hugo? ¿Por qué usarlo?

### Qué es Hugo

Hugo es un generador de sitios estáticos rápido y flexible escrito en Go. Permite crear sitios web rápidamente utilizando archivos de texto planos y plantillas.

### Por qué usar Hugo

- **Velocidad**: Hugo es extremadamente rápido.
- **Flexibilidad**: Soporta Markdown y tiene una gran cantidad de temas disponibles.
- **Facilidad de uso**: No necesitas conocimientos avanzados de programación para empezar.

---

# Preparativos iniciales

### Requisitos

1. Tener instalado Go
2. Tener instalado Hugo
3. Conocimientos básicos de la línea de comandos
4. Cuenta en Cloudflare para el despliegue (opcional)

### Instalación

```bash
# Instalar Hugo en macOS
brew install hugo

# Instalar Hugo en Linux
sudo apt-get install hugo
```

---

# Crear un nuevo proyecto con Hugo

```bash
hugo new site my-portfolio

cd my-portfolio

# Añadir un tema
git init
git submodule add \
    https://github.com/gurusabarish/hugo-profile.git \
    themes/hugo-profile

# Copiar el contenido de ejemplo
cp -r themes/hugo-profile/exampleSite/* .
```

---

# Personalizar el tema

### Personalización básica

- Editar archivos en `config.toml`
- Cambiar el contenido en la carpeta `content`
- Modificar estilos en la carpeta `themes/hugo-profile`

### Añadir nuevo contenido

```bash
# Crear una nueva página
hugo new posts/mi-primer-post.md
```

---

### Comandos básicos de Hugo

```bash
# Crear un nuevo proyecto
hugo new site my-portfolio

# Comprobar el sitio en local
hugo server -D

# Crear una nueva página
hugo new posts/mi-primer-post.md

# Crear una nueva página con un layout específico
hugo new posts/mi-segundo-post.md --kind blog

# Compilar el sitio
hugo

---

# Desplegar el portfolio en Cloudflare

### Configurar Cloudflare

1. Crear una cuenta en Cloudflare
2. Añadir un nuevo sitio
3. Configurar las DNS

### Desplegar usando Cloudflare Pages


* Publicar en Cloudflare Pages
* Conectar el repositorio de GitHub en Cloudflare Pages
* Seleccionar el proyecto y el branch
* Configurar el build command como `hugo`
* Configurar el output directory como `public`

---

# Conclusiones

- Hugo es una herramienta potente y flexible para crear portfolios.
- Desplegar en Cloudflare es sencillo y rápido.
- Un portfolio bien hecho puede destacar tus habilidades y trabajos.